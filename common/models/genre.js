'use strict';

module.exports = function(Genre) {
    Genre.getGenre = (query,cb) => {
        console.log("query AND body",query, cb);
        //const genreId = query.genreType; //value
        console.log("genre Id", genreId);
        Genre.find((err,data) => {
            if(err){
                return cb(null,{
                    success:false,
                    message:"Error fetching data from database"
                });
            }
            return cb(null,{
                success:true,
                message:"fetched data from database",
                data
            });
        })
        // .then((res)=>{
        //     return cb(null,{
        //         success:true,
        //         message:"fetched data from database",
        //         callback
        //     });
        // }).catch((err)=>{
        //     return cb({
        //         success:false,
        //         message:"Error fetching data from database"
        //     },null)
        // }); 
    }

    //POST
    Genre.addGenre =  (data,cb) =>{
        const payload = data.genreType;
        const genreData =   Genre.create({
            genreType: payload
        }).then((res)=>{
            return cb(null,{
                success:true,
                message:`${payload} Added`,
                res
            });
        })
        .catch((err)=>{
            return cb({
                success:false,
                message:"Error inserting data into database"
            },null)
        });
        
    }
    Genre.remoteMethod('getGenre',{
        description:"Get Genre List",
        accepts:[{
            arg: 'genreType1',
            type: 'string',
            http:{
                source:'query'
            }
        },
        {
            arg: 'body',
            type: 'object',
            http:{
                source:'body' //req.body
            }
        }],

        http:{
            path:'/getGenre',
            verb:'get'
        },
        returns:{
            arg:'response',
            type:'object',
        }
    });

    Genre.remoteMethod('addGenre',{
        description:"Add genres",
        accepts:[
            {
            arg:'data',
            type:'object',
            required:true,
            http:{
                source:'body'
            }
        }],
        returns:{
            args:'res',
            type:'object',
            root: true,
            description: '{success: true, msg: "message", data: result}'  
        },
        http:{
            path:'/addGenre',
            verb:'post'
        }
    })
};


