'use strict';

module.exports = function(Artist) {
    // Artist.getCountByGenre = (query,cb) => {
    //     const genreName = query;
    //     // console.log("Genre Response", genreName);
    //     cb(null,{
    //         genreName
    //     })
    // }

    //POST
    Artist.addArtist = (data,cb) =>{
        const payload = data.genreType;
        //const parsed = 
        //console.log(payload);
        //const parsed = JSON.stringify(payload);
        cb(null,{
            success:true,
            message:`${payload} Added`,
            data
        })
    }
    // Genre.remoteMethod('getCountByGenre',{
    //     description:"Get Genre count by Genre Type",
    //     accepts:{
    //         arg: 'genreType',
    //         type: 'string'
    //     },
    //     http:{
    //         path:'/getGenreCount',
    //         verb:'get'
    //     },
    //     returns:{
    //         arg:'response',
    //         type:'object',
    //         http:{
    //             source:'query'
    //         }
    //     }
    // });

    Artist.remoteMethod('addArtist',{
        description:"Add Artist to database",
        accepts:[
            {arg:'data',
            type:'object',
            required:true,
            http:{
                source:'body'
            }
        }],
        returns:{
            args:'response',
            type:'object',
            root: true,
            description: '{success: true, msg: "message", data: result}'  
        },
        http:{
            //path:'/addGenre',
            verb:'post'
        }
    })
};


