'use strict';

module.exports = async function(app){
    const artistSchema = await app.dataSources.mysql.autoupdate('artist').catch((err) => console.log("Error creating artist schema"));
    const genreSchema = await app.dataSources.mysql.autoupdate('genre').catch((err) => console.log("Error creating genre schema"));
    const albumSchema = await app.dataSources.mysql.autoupdate('album').catch((err) => console.log("Error creating album schema"));
    console.log("model/schema updated");
}